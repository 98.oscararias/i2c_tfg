`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 26.07.2022 23:50:59
// Design Name: 
// Module Name: top_i2c
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_i2c #(
    DATA_BITS = 8,
    ADDR_BITS = 8,
    DELAY = 1000)(
    input wire clk,
    input wire reset,
    input wire start,
    input wire [(DATA_BITS - 1):0] data_in,
    input wire [(ADDR_BITS - 1):0] addr_in,
    output reg i2c_sda,
    output reg i2c_scl,
    output reg ready_out
    );
    
    wire i2c_clk;
    
    i2c_clk_divider #(
        .DELAY(DELAY)
        ) DIV_CLK(
          .reset(reset),
          .ref_clk(clk),
          .i2c_clk(i2c_clk)
    );

    i2c_master #(
        .DATA_BITS(DATA_BITS), 
        .ADDR_BITS(ADDR_BITS)
        ) master(
            .clk(i2c_clk),
            .reset(reset),
            .start(start),
            .addr(addr_in),
            .data(data_in),
            .i2c_sda(i2c_sda),
            .i2c_scl(i2c_scl),
            .ready(ready_out)
    );

endmodule
