`timescale 1ns / 1ps 
`default_nettype none
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.07.2022 23:19:13
// Design Name: 
// Module Name: i2c_fifo_master
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module i2c_master #(
    DATA_BITS = 8,
    ADDR_BITS = 8
) (
    input wire clk,
    input wire reset,
    input wire start,//pndng
    input wire [DATA_BITS-1:0] data,
    input wire [ADDR_BITS-1:0] addr,
    output reg i2c_sda,
    output wire i2c_scl,
    output wire ready //pop_mcb
);

  localparam STATE_IDLE = 0;
  localparam STATE_START = 1;
  localparam STATE_ADDR = 2;
  localparam STATE_RW = 3;
  localparam STATE_WACK = 4;
  localparam STATE_DATA = 5;
  localparam STATE_WACK2 = 6;
  localparam STATE_STOP = 7;
  localparam STATE_ACK_ADDR = 8;


  reg [7:0] state;
  reg [DATA_BITS-1:0] count;
  reg i2c_scl_enable = 0;
  reg [DATA_BITS-1:0] saved_data;
  reg [ADDR_BITS-1:0] saved_addr;

  assign i2c_scl = (i2c_scl_enable == 0) ? 1 : ~clk;
  assign ready = ((reset == 0) && (state == STATE_IDLE)) ? 1 : 0;

  always @(negedge clk) begin
    if (reset == 1) begin
      i2c_scl_enable <= 0;
    end else begin
      if ((state == STATE_IDLE) || (state == STATE_START) || (state == STATE_STOP)) begin
        i2c_scl_enable <= 0;
      end else begin
        i2c_scl_enable <= 1;
      end
    end
  end


  always @(posedge clk) begin
    if (reset == 1) begin
      state   <= STATE_IDLE;
      i2c_sda <= 1;
      count   <= {(DATA_BITS - 1){1'b0}};
    end else begin
      case (state)
        STATE_IDLE: begin  // espera
          i2c_sda <= 1;
          if (start) begin 
            state   <= STATE_START;
            saved_addr <= addr;
            saved_data <= data;
           end else state  <= STATE_IDLE;
        end
        STATE_START: begin  //inicio
          i2c_sda <= 0;
          state   <= STATE_ADDR;
          count   <= (ADDR_BITS - 1);
        end
        STATE_ADDR: begin
          i2c_sda <= saved_addr[count];
          if (count == 0) state <= STATE_RW;
          else count <= (count - 1);
        end
        STATE_RW: begin
          i2c_sda <= 1;
          state   <= STATE_WACK;
        end
        STATE_WACK: begin
          state <= STATE_DATA;
          count <= (DATA_BITS - 1);
        end
        STATE_DATA: begin
          i2c_sda <= saved_data[count];
          if (count == 0) state <= STATE_WACK2;
          else count <= (count - 1);
        end
        STATE_WACK2: begin
          state <= STATE_STOP;
        end
        STATE_STOP: begin
          i2c_sda <= 1;
          state   <= STATE_IDLE;
        end

        default: begin
          state   <= STATE_IDLE;
          i2c_sda <= 1;
          count   <= 8'h00;
        end  //default end
      endcase
    end  //else end
  end  //always end
endmodule
