`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.07.2022 23:19:13
// Design Name: 
// Module Name: i2c_fifo_master
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module i2c_tb ();
  //INs
  reg  clk;
  reg  reset;
  reg  start;
  reg [7:0] addr_in;
  reg [7:0] data_in;

  //OUTs
  wire i2c_sda;
  wire i2c_scl;
  wire ready_out;

//  //CONECTs
//  wire i2c_clk;

//  //instances
//  i2c_clk_divider #(
//    .DELAY(1000)
//  ) DIV_CLK (
//      .reset(reset),
//      .ref_clk(clk),
//      .i2c_clk(i2c_clk)
//  );

//  i2c_master #(
//    .DATA_BITS(8), .ADDR_BITS(7)
//    ) master(
//        .clk(i2c_clk),
//        .reset(reset),
//        .start(start),
//        .addr(addr_in),
//        .data(data_in),
//        .i2c_sda(i2c_sda),
//        .i2c_scl(i2c_scl),
//        .ready(ready_out)
//    );

    top_i2c #(
        .DELAY(1000),
        .DATA_BITS(8),
        .ADDR_BITS(8)
        )DUT(
            .clk(clk),          
            .reset(reset),
            .start(start),        
            .data_in(data_in),
            .addr_in(addr_in),
            .i2c_sda(i2c_sda),      
            .i2c_scl(i2c_scl),      
            .ready_out(ready_out)
    );

  initial begin
    clk = 0;
    forever begin
      clk = #5 ~clk;
    end
  end
//  //WR
//  initial begin
//    reset = 1;
//    #9500;
//    reset = 0;
    
//    @(posedge ready_out);
//    addr_in = 8'h50;
//    data_in = 8'hAA;
//    start = 1;
//    @(posedge ready_out);
//    addr_in = 8'h55;
//    data_in = 8'h0F;
//    start = 1;
//    @(posedge ready_out);
//    addr_in = 8'h55;
//    data_in = 8'hF0;
//    start = 1;
//    @(posedge ready_out);
//    $finish;
//  end
  
  //RD
  initial begin
    reset = 1;
    #20000;
    reset = 0;
    
    @(posedge ready_out);
    addr_in = 8'h50;
    data_in = 8'hAA;
    start = 1;
    @(posedge ready_out);
    addr_in = 8'h55;
    data_in = 8'h0F;
    start = 1;
    @(posedge ready_out);
    addr_in = 8'h05;
    data_in = 8'hF0;
    start = 1;
    @(posedge ready_out);
    $finish;
  end

endmodule
