//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.07.2022 23:19:13
// Design Name: 
// Module Name: i2c_fifo_master
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module i2c_clk_divider #(
    DELAY = 1000
) (
    input  wire reset,
    input  wire ref_clk,
    output reg  i2c_clk
);

  reg [9:0] count;

  initial i2c_clk = 0;
  initial count = 9'b0;

  always @(posedge ref_clk) begin
    if (count == ((DELAY / 2) - 1)) begin
      i2c_clk = ~i2c_clk;
      count   = 0;
    end else begin
      count = count + 1;
    end
  end
endmodule
